Camel Router Spring Project
===========================

To build this project use

    mvn install deploy

To run this example either embed the jar inside Spring
or to run the route from within Maven try

    mvn camel:run --fabric8

For more help see the Apache Camel documentation

    http://camel.apache.org/
