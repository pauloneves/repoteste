package br.com.afferolab.examples.breadcrumb_process;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ProcessOrderRoute extends SpringRouteBuilder {

		@Override
		public void configure() throws Exception {
			from("activemq:pedidos?requestTimeoutCheckerInterval=10").routeId("ProcessOrderRoute")
			.log("${header.breadcrumbId} - Begin ProcessOrder ${date:now:yyyy-MM-dd HH:mm:ss}")
			.split().tokenizeXML("item")
			.to("activemq:pedido")
			.log("${header.breadcrumbId} - End ProcessOrder onDate ${date:now:yyyy-MM-dd HH:mm:ss}");
		}

                private void example() {
                    System.out.println("Apenas um exemplo");
                }
}
