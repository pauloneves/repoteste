package br.com.afferolab.examples.breadcrumb_process;

import java.util.UUID;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyService {
	private Logger log = LoggerFactory.getLogger(MyService.class); 
	private static int counter;
	 
	
	public void createProcessId(Exchange exchange) {
		
		String uuid = UUID.randomUUID().toString();
		
		exchange.getIn().setHeader("ProcessId", uuid);
		if (exchange.getPattern().isOutCapable()) {
			exchange.getOut().setHeaders(exchange.getIn().getHeaders());			
			exchange.getOut().setAttachments(exchange.getIn().getAttachments());
			exchange.getOut().setBody(exchange.getIn().getBody());
		}
		
	}
		
	public void process(Exchange exchange) {
		String processId = (String) exchange.getIn().getHeader(Exchange.BREADCRUMB_ID);
		
		log.info("[MYSERVICE - PROCESS] ProcessId:" + processId);
	}
	
    
    public String buildCombinedResponse() {
        return "Response[" + counter + "]";
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        return sb.toString();
   }
}
