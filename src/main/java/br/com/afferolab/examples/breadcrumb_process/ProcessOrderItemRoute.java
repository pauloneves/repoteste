package br.com.afferolab.examples.breadcrumb_process;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ProcessOrderItemRoute extends SpringRouteBuilder {

		@Override
		public void configure() throws Exception {
			from("activemq:pedido?requestTimeoutCheckerInterval=10").routeId("ProcessOrderItemRoute")
			.log("${header.breadcrumbId} - Begin ProcessOrderItem ${date:now:yyyy-MM-dd HH:mm:ss}")
			.to("log:br.com.afferolab.examples.breadcrumb?showAll=true&multiline=true")
			.log("Process ${body}")
			.log("${header.breadcrumbId} - End ProcessOrderItem ${date:now:yyyy-MM-dd HH:mm:ss}");
		}
}
